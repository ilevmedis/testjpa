package com.example.testjpa.testitem;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ActiveProfiles("ci")
public class TestItemServiceTest {
  @Autowired
  private TestItemService testItemService;

  private static TestItem getTestItem() {
    TestItem item = new TestItem();
    item.setDescr("descr");
    item.setMnemonic("mnemonic");
    return item;
  }

  @BeforeEach
  public void clearDatabase() {
    testItemService.deleteAll();
  }

  @Test
  public void whenSave_thenReturnConsistentFinItem() {
    TestItem testItem = getTestItem();

    TestItem savedFinItem = testItemService.save(testItem);

    assertThat(testItem.getDescr()).isEqualTo(savedFinItem.getDescr());
  }

  @Test
  public void whenFindById_thenReturnFinItem() {
    TestItem testItem = getTestItem();
    TestItem savedFinItem = testItemService.save(testItem);

    Optional<TestItem> optionallyRetrievedItem = testItemService.findById(testItem.getTestItemId());
    assertThat(optionallyRetrievedItem.isPresent()).isTrue();
  }

  @Test
  public void whenFindById_thenReturnNothing() {
    TestItem testItem = getTestItem();
    TestItem savedTestItem = testItemService.save(testItem);

    Optional<TestItem> optionallyRetrievedItem = testItemService.findById(testItem.getTestItemId() + 100);
    assertThat(optionallyRetrievedItem.isPresent()).isFalse();
  }

  @Test
  public void whenFindAll_thenReturnAllItems() {
    TestItem testItem = getTestItem();
    testItemService.save(testItem);

    TestItem otherTestItem = getTestItem();
    testItemService.save(otherTestItem);

    Iterable<TestItem> finItems = testItemService.findAll();
    assertThat(finItems).asList().hasSize(2);
  }
}
