package com.example.testjpa.testitem;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ActiveProfiles("ci")
public class TestUidServiceTest {
  @Autowired
  private TestUidService testUidService;

  private static TestUid getTestItem() {
    TestUid item = new TestUid();
    item.setDescr("descr");
    item.setMnemonic("mnemonic");
    return item;
  }

  @BeforeEach
  public void clearDatabase() {
    testUidService.deleteAll();
  }

  @Test
  public void whenSave_thenReturnConsistentItem() {
    TestUid testItem = getTestItem();

    TestUid savedItem = testUidService.save(testItem);

    assertThat(testItem.getDescr()).isEqualTo(savedItem.getDescr());
  }

  @Test
  public void whenFindById_thenReturnItem() {
    TestUid testItem = getTestItem();
    TestUid savedItem = testUidService.save(testItem);

    Optional<TestUid> optionallyRetrievedItem = testUidService.findById(testItem.getTestItemId());
    assertThat(optionallyRetrievedItem.isPresent()).isTrue();
  }

  @Test
  public void whenFindById_thenReturnNothing() {
    TestUid testItem = getTestItem();
    TestUid savedTestItem = testUidService.save(testItem);

    Optional<TestUid> optionallyRetrievedItem = testUidService.findById(UUID.randomUUID());
    assertThat(optionallyRetrievedItem.isPresent()).isFalse();
  }

  @Test
  public void whenFindAll_thenReturnAllItems() {
    TestUid testItem = getTestItem();
    testUidService.save(testItem);

    TestUid otherTestItem = getTestItem();
    testUidService.save(otherTestItem);

    Iterable<TestUid> items = testUidService.findAll();
    assertThat(items).asList().hasSize(2);
  }
}
