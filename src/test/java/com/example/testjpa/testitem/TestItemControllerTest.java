package com.example.testjpa.testitem;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static com.example.testjpa.testitem.TestItemController.FIND_BY_DESCR_PATH;
import static com.example.testjpa.testitem.TestItemController.TEST_ITEM_PATH;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TestItemController.class)
@AutoConfigureMockMvc
@ActiveProfiles("ci")
public class TestItemControllerTest {
  @Autowired
  private MockMvc mvc;
  @MockBean
  TestItemService testItemService;
  @Autowired
  private ObjectMapper mapper;

  @Test
  public void givenTestItemList_whenGetTestItemsByDescr_thenReturnJsonArray() throws Exception {
    //arrange
    String testDescr = "testDescr";
    TestItem testItem1 = new TestItem();
    testItem1.setTestItemId(10);
    testItem1.setDescr("descr1");
    TestItem testItem2 = new TestItem();
    testItem2.setTestItemId(20);
    testItem2.setDescr("descr2");
    List<TestItem> finItems = List.of(testItem1, testItem2);
    given(testItemService.findByDescrContains(testDescr)).willReturn(finItems);
    //act-assert
    MvcResult result = mvc.perform(get(FIND_BY_DESCR_PATH)
            .contentType(MediaType.APPLICATION_JSON)
            .param("descr", testDescr))
            .andDo(MockMvcResultHandlers.print())
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].testItemId").value(testItem1.getTestItemId()))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].descr").value(testItem1.getDescr()))
            .andExpect(MockMvcResultMatchers.jsonPath("$[1].testItemId").value(testItem2.getTestItemId()))
            .andExpect(MockMvcResultMatchers.jsonPath("$[1].descr").value(testItem2.getDescr()))
            .andReturn();
  }

  @Test
  public void givenTestItem_whenAddTestItem_thenTestItemAdded() throws Exception {
    //arrange
    TestItem testItem1 = new TestItem();
    testItem1.setTestItemId(10);
    testItem1.setDescr("descr1");

    given(testItemService.save(any(TestItem.class))).willReturn(testItem1);

    //act-assert
    RequestBuilder requestBuilder = MockMvcRequestBuilders
            .post(TEST_ITEM_PATH)
            .accept(MediaType.APPLICATION_JSON)
            .content(mapper.writeValueAsString(testItem1))
            .contentType(MediaType.APPLICATION_JSON);

    MvcResult result = mvc.perform(requestBuilder)
            .andDo(MockMvcResultHandlers.print())
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.jsonPath("testItemId").value(testItem1.getTestItemId()))
            .andExpect(MockMvcResultMatchers.jsonPath("descr").value(testItem1.getDescr()))
            .andReturn();
  }
}
