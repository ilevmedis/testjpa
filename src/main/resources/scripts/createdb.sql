create table test_item(
    testitemid number(38),
    descr varchar2(200),
    mnemonic varchar2(20),
    barcode varchar2(20),
    itemtypeindicator number(38),
    isactive varchar2(1),
    dateCreated date,
    dateModified date,
    version number(38)
);
alter table test_item add constraint test_item_pk primary key(testitemid);
create sequence test_item_seq start with 1 increment by 1 nocache;

create table test_uid(
    testitemid varchar2(36),
    descr varchar2(200),
    mnemonic varchar2(20),
    barcode varchar2(20),
    itemtypeindicator number(38),
    isactive varchar2(1),
    dateCreated date,
    dateModified date,
    version number(38)
);
alter table test_uid add constraint test_uid_pk primary key(testitemid);
