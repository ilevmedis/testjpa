package com.example.testjpa.testitem;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TestItemRepository extends CrudRepository<TestItem, Integer> {
  List<TestItem> findByDescrContains(String descr);
}
