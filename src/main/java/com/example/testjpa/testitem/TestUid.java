package com.example.testjpa.testitem;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
@Table(name = "test_uid")
public class TestUid extends AuditedEntity {
  @Id
  @GeneratedValue
  @Column(name = "testitemid")
  private UUID testItemId;
  @Column(nullable = false)
  private String descr;
  @Column(name = "isactive", nullable = false)
  private String isActive = "1";
  @Column(name = "itemtypeindicator", nullable = false)
  private Integer itemTypeIndicator = 1;
  @Column(nullable = false)
  private String mnemonic;
  @Column
  private String barcode;
}
