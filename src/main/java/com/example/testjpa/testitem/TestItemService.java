package com.example.testjpa.testitem;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TestItemService {
  private final TestItemRepository testItemRepository;

  public TestItem save(TestItem item) {
    return testItemRepository.save(item);
  }

  public void delete(TestItem fin_Item) {
    testItemRepository.delete(fin_Item);
  }

  public Optional<TestItem> findById(Integer id) {
    return testItemRepository.findById(id);
  }

  List<TestItem> findByDescrContains(String descr) {
    return testItemRepository.findByDescrContains(descr);
  }

  public Iterable<TestItem> findAll() {
    return testItemRepository.findAll();
  }

  public void deleteAll() {
    testItemRepository.deleteAll();
  }

/*
    public Fin_Item findByIdOrThrowException(long id) throws NotFoundException {
        Optional<Fin_Item> fin_Item = findById(id);
        if (!fin_Item.isPresent()) {
            throw new NotFoundException("Fin_Item With Id {" + id + "} Not Found");
        }
        return fin_Item.get();
    }
*/

/*
    public Page<Fin_Item> findAll(Pageable pageable) {
        return fin_ItemRepository.findAll(pageable);
    }
*/
}
