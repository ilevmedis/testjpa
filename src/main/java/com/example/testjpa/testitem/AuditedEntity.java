package com.example.testjpa.testitem;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Version;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class AuditedEntity implements Serializable {
  @Column(name = "datecreated")
  private Date dateCreated;
  @Column(name = "datemodified")
  private Date dateModified;
  @Column(name = "version")
  @Version
  private int version;

  @PrePersist
  public void setCreatedOn() {
    setDateCreated(new Date());
    setDateModified(new Date());
  }

  @PreUpdate
  public void setUpdatedOn() {
    setDateModified(new Date());
  }
}
