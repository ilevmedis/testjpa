package com.example.testjpa.testitem;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TestItemController {
  public static final String TEST_ITEM_PATH = "/finitem";
  public static final String FIND_BY_DESCR_PATH = TEST_ITEM_PATH + "/findByDescr";
  public static final String FIND_BY_ID_PATH = TEST_ITEM_PATH + "/findById";
  @Autowired
  private TestItemService testItemService;
  @Autowired
  ObjectMapper objectMapper;

  @GetMapping(FIND_BY_ID_PATH)
  public ResponseEntity<?> getTestItemById(@RequestParam(value = "testItemId") Integer testItemId) {
    return testItemService.findById(testItemId)
            .map(ResponseEntity::ok)
            .orElseGet(() -> ResponseEntity.notFound().build());
  }

  @GetMapping(FIND_BY_DESCR_PATH)
  public ResponseEntity<?> getFinItemsByDescr(@RequestParam(value = "descr") String descr) {
    List<TestItem> finItems = testItemService.findByDescrContains(descr);
    return ResponseEntity.ok(finItems);
  }

  @PostMapping(TEST_ITEM_PATH)
  public ResponseEntity<?> addTestItem(@RequestBody TestItem testItem) {
    TestItem saveItem = testItemService.save(testItem);
    return ResponseEntity.ok(saveItem);
  }
}
