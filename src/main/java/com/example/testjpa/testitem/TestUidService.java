package com.example.testjpa.testitem;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class TestUidService {
  private final TestUidRepository testUidRepository;

  public TestUid save(TestUid testUid) {
    return testUidRepository.save(testUid);
  }

  public void delete(TestUid testUid) {
    testUidRepository.delete(testUid);
  }

  public Optional<TestUid> findById(UUID id) {
    return testUidRepository.findById(id);
  }

  List<TestUid> findByDescrContains(String descr) {
    return testUidRepository.findByDescrContains(descr);
  }

  public Iterable<TestUid> findAll() {
    return testUidRepository.findAll();
  }

  public void deleteAll() {
    testUidRepository.deleteAll();
  }
}
