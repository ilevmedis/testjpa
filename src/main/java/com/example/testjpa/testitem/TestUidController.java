package com.example.testjpa.testitem;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class TestUidController {
  public static final String TEST_UID_PATH = "/testuid";
  public static final String FIND_BY_DESCR_PATH = TEST_UID_PATH + "/findByDescr";
  public static final String FIND_BY_ID_PATH = TEST_UID_PATH + "/findById";
  @Autowired
  private TestUidService testUidService;
  @Autowired
  ObjectMapper objectMapper;

  @GetMapping(FIND_BY_ID_PATH)
  public ResponseEntity<?> getTestUidById(@RequestParam(value = "testItemId") UUID testItemId) {
    return testUidService.findById(testItemId)
            .map(ResponseEntity::ok)
            .orElseGet(() -> ResponseEntity.notFound().build());
  }

  @GetMapping(FIND_BY_DESCR_PATH)
  public ResponseEntity<?> getFinItemsByDescr(@RequestParam(value = "descr") String descr) {
    List<TestUid> items = testUidService.findByDescrContains(descr);
    return ResponseEntity.ok(items);
  }

  @PostMapping(TEST_UID_PATH)
  public ResponseEntity<?> addTestUid(@RequestBody TestUid testItem) {
    TestUid saveItem = testUidService.save(testItem);
    return ResponseEntity.ok(saveItem);
  }
}
