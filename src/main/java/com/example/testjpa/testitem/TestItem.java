package com.example.testjpa.testitem;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
@Table(name = "test_item")
public class TestItem extends AuditedEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "test_item_seq")
  @SequenceGenerator(sequenceName = "test_item_seq", allocationSize = 1, name = "test_item_seq")
  @Column(name = "testitemid")
  private Integer testItemId;
  @Column(nullable = false)
  private String descr;
  @Column(name = "isactive", nullable = false)
  private String isActive = "1";
  @Column(name = "itemtypeindicator", nullable = false)
  private Integer itemTypeIndicator = 1;
  @Column(nullable = false)
  private String mnemonic;
  @Column
  private String barcode;
  //@OneToMany(fetch = FetchType.LAZY)
  //private List<TestItemTrans> transactions;
}
