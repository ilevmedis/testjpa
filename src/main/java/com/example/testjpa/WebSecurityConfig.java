package com.example.testjpa;

import com.example.testjpa.testitem.TestItemController;
import com.example.testjpa.testitem.TestUidController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
            .csrf().disable()
            .authorizeRequests()
            .antMatchers(
                    "/",
                    TestItemController.FIND_BY_DESCR_PATH,
                    TestItemController.FIND_BY_ID_PATH,
                    TestUidController.FIND_BY_DESCR_PATH,
                    TestUidController.FIND_BY_ID_PATH
            )
            .permitAll()
            .antMatchers(HttpMethod.POST,
                    TestItemController.TEST_ITEM_PATH,
                    TestUidController.TEST_UID_PATH
            )
            .permitAll()
            .anyRequest().authenticated()
            .and()
            .formLogin()
            .loginPage("/login").permitAll()
            .and()
            .logout().permitAll();
  }

  @Bean
  @Override
  public UserDetailsService userDetailsService() {
    UserDetails user =
            User.withDefaultPasswordEncoder()
                    .username("user")
                    .password("password")
                    .roles("USER")
                    .build();

    return new InMemoryUserDetailsManager(user);
  }
}
